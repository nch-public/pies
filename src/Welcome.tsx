/**
 * This component was deliberately designed to be simple and unstyled, so as not to
 * influence your design decisions. Since we use Material UI in our project, we have
 * pre-installed it for you, but you are free to use any other interface component
 * libraries that you are comfortable with.
 */
const Welcome = () => {
  return (
    <section style={{ textAlign: "left" }}>
      <h1>Welcome!</h1>
      <p>
        We believe that whiteboard interviews and tricky algorithm questions are
        not the best way to evaluate a candidate's ability to do the job. After
        all, you won't be working on a whiteboard once you're hired. So, we've
        created this interview process to give you a more realistic opportunity
        to show us your skills.
      </p>
      <h2>Goal</h2>
      <p>
        Your mission, should you choose to accept it, is to build a page for
        showing a list of pies. Who doesn't like pie? The basic requirements
        are:
      </p>
      <ul>
        <li>Use React for the ui library.</li>
        <li>
          The front end will make a request to a server to retrieve a list of
          pies.
        </li>
        <li>The front end will render the results in a list.</li>
        <li>
          Implement at least one way to filter the list. For example, a search
          box or a checkbox for excluding pies with nuts.
        </li>
      </ul>
      <h2>Details</h2>
      <p>
        We've set up a starter kit for the app to help you get started quickly
        and easily. This includes everything you need to get up and running,
        such as pre-installed packages and a pre-configured build script.
        However, <b>feel free to customize the app to your liking</b> by
        substituting other libraries or making other changes as needed. In fact,
        tasteful creativity may earn you extra credit!
      </p>
      <p>Here are the main libraries we've pre-installed for you:</p>
      <ul>
        <li>
          <b>React</b>
        </li>
        <li>
          <b>Material UI</b>
        </li>
        <li>Build-time stuff like vite, typescript, eslint, etc.</li>
      </ul>
      <p>Here's an overview of the existing files:</p>
      <ul>
        <li>
          <b>src/App.tsx</b> is the primary component for the client side. Make
          changes there, and feel free to add extra components if you like.
        </li>
        <li>
          <b>db.json</b> contains the data which drives the server side. After
          starting the server, you can make a GET "http://localhost:5005/pies"
          to get the list of pies
        </li>
      </ul>
    </section>
  );
};

export default Welcome;
