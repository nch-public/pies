import "./App.css";
import Welcome from "./Welcome";

// After starting the development server, make a GET request to this url to
//  retrieve a list of pies
const pieUrl = "http://localhost:5005/pies";

const App = () => {
  return (
    <div className="App">
      {/* TODO: Replace the welcome component with your code */}
      <Welcome />
    </div>
  );
};

export default App;
