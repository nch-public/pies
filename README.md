To get setup:

1. Clone the project via `git clone https://gitlab.com/nch-public/pies.git`
2. Change directory into the project: `cd pies`
3. Install dependencies: `npm install`

To run the app:

1. Start the development server `npm run server`
2. In another terminal window, start the client `npm start`

To submit your code:

1. Delete the node_modules directory.
2. Zip the root directory file (unless you chose a different name during cloning, it's called 'pies').
3. Email the zipped file back to Natalie.
